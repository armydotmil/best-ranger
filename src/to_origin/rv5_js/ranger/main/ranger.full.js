$(document).ready(function() {
	$('.innerNav li').hover(
	  function () {
	    $(this).children('.subLink').addClass("hover");
	  },
	  function () {
	    $(this).children('.subLink').removeClass("hover");
	  }
	);
	
	jQuery("#addThis").show();
    jQuery("#moreShare").hide();
    jQuery("#share").click(function() {
        jQuery("#addThis").fadeIn(700);
        jQuery("#tops").fadeIn(700);
    });
    jQuery(".all").click(function() {
        jQuery("#tops").hide();
        jQuery("#moreShare").fadeIn(700);
    });
    jQuery("#shareClose").click(function() { 
        jQuery("#addThis").fadeOut(700, function(){
            jQuery("#moreShare").hide();
            jQuery("#tops").show();
        });
    });	
});