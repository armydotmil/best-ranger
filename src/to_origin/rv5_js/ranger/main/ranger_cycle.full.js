$(document).ready(function() {
	//----- image slideshow components on homepage and competition page -----//	
	$('#images').cycle({ 
		fx:     'fade',
		speed:   2000,
		timeout: 6000
	});
});