$(document).ready(function() {

    //Adds more or less button to expand and collapse paragraphs
	$("#benning-button-more").click(function() {
		$(".benning-more").show();
		$(this).hide();
		$("#benning-button-less").show();
	});

	$("#benning-button-less").click(function() {
		$(".benning-more").hide();
		$(this).hide();
		$("#benning-button-more").show();
	});
	
	$("#mountain-button-more").click(function() {
		$(".mountain-more").show();
		$(this).hide();
		$("#mountain-button-less").show();
	});

	$("#mountain-button-less").click(function() {
		$(".mountain-more").hide();
		$(this).hide();
		$("#mountain-button-more").show();
	});
	
	$("#florida-button-more").click(function() {
		$(".florida-more").show();
		$(this).hide();
		$("#florida-button-less").show();
	});

	$("#florida-button-less").click(function() {
		$(".florida-more").hide();
		$(this).hide();
		$("#florida-button-more").show();
	});
	
	
	
	$("#images").cycle({
		fx: "fade",
		speed: 2e3,
		timeout: 6e3
	});

});
