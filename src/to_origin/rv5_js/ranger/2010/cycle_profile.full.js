$(document).ready(function(){
			$('#slide').cycle({ 
    			fx:    'fade', 
    			speed:    800, 
    			timeout:  4000 
			});

  			var $teams = $('#teams_comp').find('li');
  			
  			var n = $teams.length;
  			var random1 = Math.floor( Math.random()*n );
  			var random2 = Math.floor( Math.random()*n );
  			while(random1==random2){random2 = Math.floor( Math.random()*n );}
  			$teams.hide();
  			$teams.eq(random1).fadeIn("slow");
  			$teams.eq(random2).fadeIn("slow");
		});
