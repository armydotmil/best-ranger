$(document).ready(function() {
	$('ul#filter a').click(function() {
		$(this).css('outline','none');
		$('ul#filter .current').removeClass('current');
		$(this).parent().addClass('current');
		
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
				
		if(filterVal == 'all') {
			$('ul#teams li.hidden').show().removeClass('hidden');
		} else {
			
			$('ul#teams li').each(function() {
				if(!$(this).hasClass(filterVal)) {
					$(this).hide().addClass('hidden');
				} else {
					$(this).show().removeClass('hidden');
				}
			});
		}
		
		return false;
	});
});
